//
//  AppDelegate.h
//  MJiOSCore
//
//  Created by Joan Martin on 19/05/16.
//  Copyright © 2016 MobileJazz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

